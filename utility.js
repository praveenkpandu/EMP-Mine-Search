
var fs = require('fs');
var util = require('util');

/*
Find the last point of the grid the robot can reach
travelling in only one direction. 
*/
let calculateBoundary = async (safetyLimit) => {
    i = 0
    while (Math.abs(i).toString().split('').map(Number)
    .reduce((a,b) => a+b) <= safetyLimit) {
        i++;
      }
      return i;
    //   console.log("Boundary of the grid - " + i );
}

//Function to calculate the sum of numbers
let sumUpNumbers = async () => {
    var sum = 0 ;
    for (var i = 0; i < arguments.length; i++) {
        sum += Math.abs(arguments[i]).toString().split('').reduce((a,b) => a+b);
    }
    return sum;
}

function isGridSafe() {
    let sum = 0 ;
    for (var i = 1; i < arguments.length; i++) {
        sum += Math.abs(arguments[i]).toString().split('').map(Number).reduce((a,b) => a+b);
        if (sum > arguments[0]) {
            return false;
        }
    }
    return true;
}


let saveSafeGrids = async (data) => {
    fs.writeFileSync('./SafeGrids.json', util.inspect(data) , 'utf-8');
}

module.exports = {calculateBoundary, sumUpNumbers, isGridSafe, saveSafeGrids}