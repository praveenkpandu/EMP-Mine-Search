/*
Description of the problem 
============================
There is a robot which can move around on a grid. 
The robot is placed at point (0,0). From (x, y) the 
robot can move to (x+1, y), (x-1, y), (x, y+1), and 
(x, y-1). Some points are dangerous and contain EMP 
Mines. To know which points are safe, we check whether 
the sum digits of abs(x) plus the sum of the digits of 
abs(y) are less than or equal to 23. For example, the 
point (59,75) is not safe because 5 + 9 + 7 + 5 = 26, 
which is greater than 23. The point (-51, -7) is safe 
because 5 + 1 + 7 = 13, which is less than 23
*/
var utility = require('./utility');

//Environment setup
const startPoint = [0,0];
const safeMineLimit = 23;


let createGrids = async (boundaryLimit) => {
    var gridArray = new Map();
    const lowestPoint = -1*boundaryLimit;

    for (var i = lowestPoint; i < boundaryLimit ; i ++){
        for (var j = lowestPoint; j < boundaryLimit ; j ++){
            const newKey = i.toString() + "," + j.toString()
            gridArray.set(newKey, 'f')
        }
    }
    return gridArray;
}


let getGridKey = (gridPoint) => {
    const newKey = gridPoint[0].toString() + "," + gridPoint[1].toString()
    return newKey;
}

let navigateGrids = async (startPoint, safeMineLimit) => {
    const mineSafeDepth = await utility.calculateBoundary(safeMineLimit);
    let grid = await createGrids(mineSafeDepth);

    let safeGrid = [];
    let possibleMoves = [];
    const allowedSteps = [[0,1],[1,0],[-1,0],[0,-1]];
    
    possibleMoves.push(startPoint);

    while (possibleMoves.length > 0) {
        let currentGrid = possibleMoves.pop();
        safeGrid.push(currentGrid);
        console.log("new Point added" , currentGrid);

        allowedSteps.forEach(element => {
            const newPoint = [Number(currentGrid[0]) + element[0] , Number(currentGrid[1])+ element[1]]
            const newPointStr = getGridKey(newPoint)

            if (grid.get(newPointStr) === 'f') {
                const newPointStr = (newPoint[0]).toString() + "," + (newPoint[1]).toString();
                grid.set(newPointStr, 't');

                if (utility.isGridSafe(safeMineLimit,newPoint[0],newPoint[1]) === true){
                    possibleMoves.push(newPoint);
                }
            }
                
        });
    }

    return safeGrid;
}

//Execute function immediately
(async () => {
    await navigateGrids(startPoint, safeMineLimit)
    .then((data) => {
        console.log("Total number of safe grids", data.length);
        utility.saveSafeGrids(data);
    });
})();


