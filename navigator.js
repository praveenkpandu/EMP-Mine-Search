const { promisify } = require('util');

// Awaitable timers are available in Node.js 15.x+
// For Node.js 12 and 14, use promisify(setTimeout)
const { setTimeout } = require('timers/promises');

module.exports = async ({ location, areaLimit }) => {
    let safeGridCounter = 0;
    let visitedGrid = [];
    let mines = [];

    let width = 0
    let breadth = 0

    console.log("1111");

    for (let m = (areaLimit * -1); m < areaLimit; m++) {
        width++;
        breadth = 0;
        for (let n = (areaLimit * -1); n < areaLimit; n++) {
            breadth++;
            const newLocation = [m, n]
            if (gridIsSafe(newLocation)) {
                if (visitedGrid.includes(newLocation)) {
                    //Already present in the list
                } else {
                    visitedGrid.push(newLocation);
                }
                safeGridCounter++;
            } else {
                if (mines.includes(newLocation)) {
                    //Already present in the list
                } else {
                    mines.push(newLocation);
                }
            }
        }
    }
    // console.log(safeGridCounter);
    // console.log("Mines length- " + mines.length);
    // console.log("Visited Grid - " + visitedGrid.length);
    // writeListToFile('VisitedGrid.txt', visitedGrid);
    // writeListToFile('mines.txt', mines);
    // console.log("Square breadth width ", + breadth + "," + width)

    return safeGridCounter;
};